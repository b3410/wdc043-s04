public class Main {
    //public String sample = "Sample";
    public static void main(String[] args) {
        /*Main newSample = new Main();
        System.out.println(newSample.sample);*/

        Car car1 = new Car();
//            System.out.println(car1.brand);
//            System.out.println(car1.make);
//            System.out.println(car1.price);
//            car1.make = "Veyron";
//            car1.brand = "Bugatti";
//            car1.price = 2000000;
//            System.out.println(car1.brand);
//            System.out.println(car1.make);
//            System.out.println(car1.price);

            //Each instance of a class should be independent from one another especially theor properties. However, since coming from the same class, they may have the same methods.

            Car car2 = new Car();
//            car2.make = "Tamaraw FX";
//            car2.brand = "Toyota";
//            car2.price = 450000;
//            System.out.println(car2.brand);
//            System.out.println(car2.make);
//            System.out.println(car2.price);
            //car2.owner = "Mang Jose"; we cannot add new property not described in the class.
            /*
                Create a new instance of the Car Class and save it in a variable called car 3.
                Access the properties of the instance an update its values.
                make = String
                brand = String
                print = int

                You can come up with your own values.
                Print the values of each property of the instance
            */
           Car car3 = new Car();
//            car3.make = "GTR 35";
//            car3.brand = "Nissan";
//            car3.price = 7350000;
//            System.out.println(car3.brand);
//            System.out.println(car3.make);
//            System.out.println(car3.price);
             car3.start();
             car1.start();
             car2.start();
             car1.setMake("Veyron");
             System.out.println(car1.getMake());
             car2.setMake("Lamborghini");
             car2.setBrand("Hucaran");
             car2.setPrice(210104);
            System.out.println(car2.getMake());
            System.out.println(car2.getBrand());
            System.out.println(car2.getPrice());

            //When creating a new instance of a class:
            //new keyword allows us to create a new instance
            //Car() - constructor of our class - w/o arguments - default constructor - which java can define for us.

        System.out.println("Details of Driver1:");
        Driver driver1 = new Driver("Vin Diesel", 54, "America");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        Car car4 = new Car("Corolla", "Toyota", 5000, driver1);
        System.out.println(car4.getMake());
        System.out.println(car4.getBrand());
        System.out.println(car4.getPrice());
        System.out.println(car4.getCarDriver().getName());

        Animal animal1 = new Animal("chuchu", "brown");
        System.out.println(animal1.getName());
        System.out.println(animal1.getColor());
        animal1.call();
        Dog newDog = new Dog();
        System.out.println(newDog.getName());
        newDog.setName("Balto");
        System.out.println(newDog.getName());
        Dog newDog2 = new Dog("whitey", "white", "huskey");
        System.out.println(newDog.getName());
        newDog2.call();
        System.out.println(newDog2.getDogBreed());
        newDog2.greet();

    }
}