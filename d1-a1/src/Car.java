public class Car {

    //An object is an idea of a real word object.
    //A class is the code that describes the object.
    //An instance is a tangible copy of an idea instantiated or created from a class.
    //Attributes and Methods'
    //public - the variable in the class is accessible in anywhere in application
    //Class attributes/properties must not be public, they should only be accessed and set vua class methods called setters and getters.
    //private - limit the access and ability to set a variable/method to its class.
    //setters are public methods which will allow us set the value of the attribute of an instance
    //getters are public methods will allow us to get the value of the attribute of an instance.
    private String make;
    private String brand;
    private int price;
    //Add a  driver variable to add a property which is an object/instance of a Driver Class.
    private Driver carDriver;

    //Methods are functions of an object allows us to perform a certain tasks
    //void - means that the function does not return anything. Because in Java, a function/methods return dataType must be declared.
    public void start(){
        System.out.println("Vroom! Vroom!");
    }
    //parameters in Java needs its dataType declared
//    public void setMake(String makeParams){
//        //this keyword refers to the object where the constructor or setter is.
//        this.make = makeParams;
//    }
//
//    //make getter
//    public String getMake(){
//        return this.make;
//    }
//
//    public void setBrand(String brandParams){
//        this.brand = brand;
//    }
//    public String getBrand(){
//        return this.brand;
//    }
//
//    public void setPrice(int priceParams){
//        this.price = price;
//    }
//    public String getPrice(){
//        return this.price;
//    }

    //to make a property read-only, don't iclude a setter function
    public void setMake(String make) {
        this.make = make;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMake() {
        return make;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    //a constructor is a method which allows us to set the initial values of an instance.
    //empty/default constructor - default constructor - allows us to create an instance with default initialized
    public  Car(){
        //empty or you can designate values instead of getting them from parameters
        //Java, actually already creates one for us, however, empty/default constructor made just by Java allows us to Java to set its own default values. If you create your own, you can set your own default values


    }
    //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance,
//        public Car(String make,String brand, int price){
//            this.make = make;
//            this.brand = brand;
//            this.price = price;

//        }
    public Driver getCarDriver() {
        return carDriver;
    }

        public void setCarDriver(Driver carDriver) {
            this.carDriver = carDriver;
        }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
